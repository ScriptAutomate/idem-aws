"""Tests for validating Rds Db Proxy Endpoints."""
import uuid

import pytest


PARAMETER = {"name": "idem-test-resource-" + str(uuid.uuid4())}


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="create")
async def test_create(hub, ctx, aws_rds_db_proxy):
    r"""
    **Test function**
    """

    # Test - create new resource
    global PARAMETER
    PARAMETER["tags"] = {"Name": PARAMETER["name"]}
    ret = await hub.exec.aws.rds.db_proxy_endpoint.create(
        ctx,
        name=PARAMETER["name"],
        db_proxy_name=aws_rds_db_proxy.get("resource_id"),
        db_proxy_endpoint_name=PARAMETER["name"],
        vpc_subnet_ids=aws_rds_db_proxy.get("vpc_subnet_ids"),
        tags=PARAMETER["tags"],
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    PARAMETER["resource_id"] = resource.get("resource_id", None)
    assert PARAMETER["resource_id"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_endpoint.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("resource_id")
    assert aws_rds_db_proxy.get("resource_id") == resource.get("db_proxy_name")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="get", depends=["create"])
async def test_get(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Invalid/Not-Found resource lookup
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_endpoint.get(
        ctx, name=PARAMETER["name"], resource_id="invalid-resource-id"
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])

    ret = await hub.exec.aws.rds.db_proxy_endpoint.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="list", depends=["create"])
async def test_list(hub, ctx):
    r"""
    **Test function**
    """

    global PARAMETER
    ret = await hub.exec.aws.rds.db_proxy_endpoint.list(ctx)
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    for resource in ret["ret"]:
        assert resource


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["list"])
async def test_update(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Update existing resource
    global PARAMETER

    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    PARAMETER["tags"] = {"Name": "idem-test-tag-updated"}
    ret = await hub.exec.aws.rds.db_proxy_endpoint.update(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        db_proxy_endpoint_name=PARAMETER["name"],
        tags=PARAMETER["tags"],
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_endpoint.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert resource
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["name"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="delete", depends=["update"])
async def test_delete(hub, ctx):
    r"""
    **Test function**
    """

    # Test - Delete existing resource
    global PARAMETER
    assert PARAMETER.get(
        "resource_id", None
    ), "The resource might not have been created"

    ret = await hub.exec.aws.rds.db_proxy_endpoint.delete(
        ctx, name="", resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert not ret["ret"]

    # Now get the resource
    ret = await hub.exec.aws.rds.db_proxy_endpoint.get(
        ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
    )
    assert ret
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert "result is empty" in str(ret["comment"])
