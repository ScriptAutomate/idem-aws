from typing import List

import pytest


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Localstack pro is unable to create Guardduty resources, so test against a real AWS endpoint",
)
async def test_list_detector(hub, ctx, aws_guardduty_detector):
    ret = await hub.exec.aws.guardduty.detector.list(ctx, name="list detectors")
    assert ret["result"], ret["comment"]
    assert isinstance(ret["ret"], List)
    assert len(ret["ret"]) == 1
    resource = ret["ret"][0]
    assert aws_guardduty_detector["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Localstack pro is unable to create Guardduty resources, so test against a real AWS endpoint",
)
async def test_get_detector(hub, ctx, aws_guardduty_detector):
    resource_id = aws_guardduty_detector["resource_id"]
    ret = await hub.exec.aws.guardduty.detector.get(ctx, resource_id=resource_id)
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert aws_guardduty_detector["resource_id"] == resource.get("resource_id")
    assert aws_guardduty_detector["finding_publishing_frequency"] == resource.get(
        "finding_publishing_frequency"
    )
    assert aws_guardduty_detector["enable"] == resource.get("enable")
    assert aws_guardduty_detector["data_sources"] == resource.get("data_sources")


@pytest.mark.asyncio
@pytest.mark.localstack(
    False,
    "Localstack pro is unable to create Guardduty resources, so test against a real AWS endpoint",
)
async def test_get_invalid_detector_id(hub, ctx):
    name = "Return empty detector"
    ret = await hub.exec.aws.guardduty.detector.get(
        ctx, resource_id="dummy_id", name=name
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get aws.guardduty.detector '{name}' result is empty" in str(ret["comment"])
