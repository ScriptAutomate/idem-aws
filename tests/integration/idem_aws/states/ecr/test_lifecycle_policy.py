import json
import time
import uuid
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-lifecycle_policy-" + str(int(time.time())),
}
RESOURCE_TYPE = "aws.ecr.lifecycle_policy"


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
async def test_present(
    hub,
    ctx,
    __test,
    aws_ecr_repository,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    registry_id = aws_ecr_repository.get("registry_id")
    repository_name = aws_ecr_repository.get("repository_name")
    policy_text = (
        '{ "rules": [ { "rulePriority": 1, "description": "Expire images older than 14 days", '
        '"selection": { "tagStatus": "untagged", "countType": "sinceImagePushed", "countUnit": "days", '
        '"countNumber": 14}, "action": {"type": "expire" } } ]}'
    )
    policy_text_standardised = hub.tool.aws.state_comparison_utils.standardise_json(
        policy_text
    )
    PARAMETER["repository_name"] = repository_name
    PARAMETER["registry_id"] = registry_id
    PARAMETER["lifecycle_policy_text"] = policy_text_standardised
    response = await hub.states.aws.ecr.lifecycle_policy.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert not response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert_policy(resource=resource, parameters=PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="create-again", depends=["present"])
async def test_create_again(
    hub,
    ctx,
    __test,
):
    # Create ecr lifecycle policy again with same resource_id and no change in state
    ret = await hub.states.aws.ecr.lifecycle_policy.present(
        ctx,
        **PARAMETER,
    )

    assert ret["result"], ret["comment"]
    assert (
        f"aws.ecr.lifecycle_policy '{PARAMETER['name']}' already exists"
        in ret["comment"]
    )
    assert ret.get("old_state") and ret.get("new_state")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-name-invalid", depends=["create-again"])
async def test_exec_get_invalid_repository(hub, ctx):
    fake_repository_name = str(uuid.uuid4())
    ret = await hub.exec.aws.ecr.lifecycle_policy.get(
        ctx,
        name=fake_repository_name,
        resource_id=fake_repository_name,
        registry_id=PARAMETER["registry_id"],
        repository_name=fake_repository_name,
    )
    assert ret["result"]
    assert hub.tool.aws.comment_utils.get_empty_comment(
        resource_type=RESOURCE_TYPE, name=fake_repository_name
    ) in str(ret["comment"])
    assert not ret["ret"]


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="exec-get-by-name", depends=["exec-get-by-name-invalid"])
async def test_exec_repository_policy_exists(hub, ctx):
    ret = await hub.exec.aws.ecr.lifecycle_policy.get(
        ctx,
        name=PARAMETER["name"],
        resource_id=PARAMETER["resource_id"],
        registry_id=PARAMETER["registry_id"],
        repository_name=PARAMETER["repository_name"],
    )
    assert ret["result"], ret["comment"]
    assert_policy(resource=ret["ret"], parameters=PARAMETER)


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="update", depends=["exec-get-by-name"])
async def test_update(
    hub,
    ctx,
    __test,
):
    global PARAMETER, RESOURCE_TYPE
    ctx["test"] = __test
    policy_text = (
        '{ "rules": [ { "rulePriority": 1, "description": "Rule 1", "selection": { "tagStatus": "tagged", '
        '"tagPrefixList": ["prod"], "countType": "imageCountMoreThan", "countNumber": 1}, '
        '"action": { "type": "expire" } }, { "rulePriority": 2, "description": "Rule 2", '
        '"selection": { "tagStatus": "tagged", "tagPrefixList": ["beta"], "countType": '
        '"imageCountMoreThan", "countNumber": 1 }, "action": { "type": "expire" } } ] }'
    )
    policy_text_standardised = hub.tool.aws.state_comparison_utils.standardise_json(
        policy_text
    )
    PARAMETER["lifecycle_policy_text"] = policy_text_standardised
    response = await hub.states.aws.ecr.lifecycle_policy.present(
        ctx,
        **PARAMETER,
    )
    assert response["result"], response["comment"]
    assert response.get("old_state") and response.get("new_state")
    resource = response.get("new_state")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource["resource_id"]
        assert (
            hub.tool.aws.comment_utils.update_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in response["comment"]
        )
    assert_policy(resource=resource, parameters=PARAMETER)


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="describe", depends=["update"])
async def test_describe(hub, ctx):
    describe_response = await hub.states.aws.ecr.lifecycle_policy.describe(ctx)
    assert describe_response[PARAMETER["resource_id"]]
    assert describe_response.get(PARAMETER["resource_id"]) and describe_response.get(
        PARAMETER["resource_id"]
    ).get("aws.ecr.lifecycle_policy.present")
    described_resource = describe_response.get(PARAMETER["resource_id"]).get(
        "aws.ecr.lifecycle_policy.present"
    )
    resource = dict(ChainMap(*described_resource))
    assert PARAMETER["registry_id"] == resource.get("registry_id")
    assert PARAMETER["repository_name"] == resource.get("repository_name")
    assert json.loads(PARAMETER["lifecycle_policy_text"]) == json.loads(
        resource.get("lifecycle_policy_text")
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="absent", depends=["describe"])
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    # Delete ECR Lifecycle Policy.
    ret = await hub.states.aws.ecr.lifecycle_policy.absent(
        ctx,
        name=PARAMETER["name"],
        repository_name=PARAMETER["repository_name"],
        resource_id=PARAMETER["resource_id"],
        registry_id=PARAMETER["registry_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert_policy(resource=resource, parameters=PARAMETER)
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already-absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.ecr.lifecycle_policy.absent(
        ctx,
        name=PARAMETER["name"],
        repository_name=PARAMETER["repository_name"],
        resource_id=PARAMETER["resource_id"],
        registry_id=PARAMETER["registry_id"],
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="none-resource", depends=["already-absent"])
async def test_absent_with_none_resource_id(hub, ctx):
    ret = await hub.states.aws.ecr.lifecycle_policy.absent(ctx, name=PARAMETER["name"])
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type=RESOURCE_TYPE, name=PARAMETER["name"]
        )[0]
        in ret["comment"]
    )


# This cleanup fixture cleans up the resource after all tests have run
@pytest.fixture(scope="module")
@pytest.mark.localstack(False)
async def cleanup(hub, ctx):
    global PARAMETER
    yield None

    if "resource_id" in PARAMETER:
        ret = await hub.states.aws.ecr.lifecycle_policy.absent(
            ctx, name=PARAMETER["name"], resource_id=PARAMETER["resource_id"]
        )
        assert ret["result"], ret["comment"]


def assert_policy(resource, parameters):
    assert parameters["name"] == resource.get("name")
    assert parameters["registry_id"] == resource.get("registry_id")
    assert parameters["repository_name"] == resource.get("repository_name")
    assert json.loads(parameters["lifecycle_policy_text"]) == json.loads(
        resource.get("lifecycle_policy_text")
    )
