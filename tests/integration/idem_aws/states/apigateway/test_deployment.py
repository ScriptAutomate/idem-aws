import copy
import time
from collections import ChainMap

import pytest

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-resource-" + str(int(time.time())),
    "description": "Idem Testing for Deployment",
}


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
async def test_present(hub, ctx, __test, aws_apigateway_integration, cleanup):
    global PARAMETER
    rest_api_id = aws_apigateway_integration.get("rest_api_id")
    PARAMETER["rest_api_id"] = rest_api_id

    ctx["test"] = __test
    present_ret = await hub.states.aws.apigateway.deployment.present(ctx, **PARAMETER)

    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    resource_id = resource["resource_id"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.apigateway.deployment",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        PARAMETER["resource_id"] = resource_id
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.apigateway.deployment",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    assert not present_ret.get("old_state") and present_ret.get("new_state")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["description"] == resource.get("description")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="describe", depends=["present"])
async def test_describe(hub, ctx):
    describe_ret = await hub.states.aws.apigateway.deployment.describe(ctx)
    resource_id = PARAMETER["resource_id"]
    assert resource_id in describe_ret
    assert "aws.apigateway.deployment.present" in describe_ret[resource_id]
    described_resource = describe_ret[resource_id].get(
        "aws.apigateway.deployment.present"
    )
    described_resource_map = dict(ChainMap(*described_resource))
    assert PARAMETER["description"] == described_resource_map.get("description")
    assert PARAMETER["rest_api_id"] == described_resource_map.get("rest_api_id")
    assert PARAMETER["resource_id"] == described_resource_map.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="update", depends=["describe"])
async def test_update(hub, ctx, __test):
    ctx["test"] = __test
    new_parameter = copy.deepcopy(PARAMETER)
    new_parameter["description"] = "Updated Description"
    ret = await hub.states.aws.apigateway.deployment.present(
        ctx,
        **new_parameter,
    )
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_update_comment(
                resource_type="aws.apigateway.deployment",
                name=new_parameter["name"],
            )[0]
            in ret["comment"]
        )
    else:
        if not hub.tool.utils.is_running_localstack(ctx):
            assert (
                hub.tool.aws.comment_utils.update_comment(
                    resource_type="aws.apigateway.deployment",
                    name=new_parameter["name"],
                )[0]
                in ret["comment"]
            )
            assert ret["result"], ret["comment"]
            assert ret.get("old_state") and ret.get("new_state")
            resource = ret["new_state"]
            assert new_parameter["description"] == resource.get("description")
            assert new_parameter["resource_id"] == resource.get("resource_id")
            assert new_parameter["rest_api_id"] == resource.get("rest_api_id")
            assert new_parameter["name"] == resource.get("name")
            PARAMETER["description"] = new_parameter["description"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["update"])
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.apigateway.deployment.get(
        ctx,
        rest_api_id=PARAMETER["rest_api_id"],
        resource_id=PARAMETER["resource_id"],
        name=PARAMETER["name"],
    )

    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["description"] == resource.get("description")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["name"] == resource.get("name")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get-not-found", depends=["present"])
async def test_get_resource_id_does_not_exist(hub, ctx):
    resource_id = "test_idem_resource_id"
    rest_api_id = "test_idem_rest_api_id"
    ret = await hub.exec.aws.apigateway.deployment.get(
        ctx,
        rest_api_id=rest_api_id,
        resource_id=resource_id,
        name=PARAMETER["name"],
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"] is None
    assert f"Get '{resource_id}' result is empty" and f"NotFoundException" in str(
        ret["comment"]
    )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent", depends=["update"])
async def test_absent_deployment(hub, ctx, __test):
    global PARAMETER
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.deployment.absent(
        ctx,
        name=PARAMETER["name"],
        rest_api_id=PARAMETER["rest_api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"], ret["comment"]
    assert ret.get("old_state") and not ret.get("new_state")
    resource = ret.get("old_state")
    assert PARAMETER["resource_id"] == resource.get("resource_id")
    assert PARAMETER["rest_api_id"] == resource.get("rest_api_id")
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["description"] == resource.get("description")
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_delete_comment(
                resource_type="aws.apigateway.deployment",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )
    else:
        assert (
            hub.tool.aws.comment_utils.delete_comment(
                resource_type="aws.apigateway.deployment",
                name=PARAMETER["name"],
            )[0]
            in ret["comment"]
        )


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(False)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    ret = await hub.states.aws.apigateway.deployment.absent(
        ctx,
        name=PARAMETER["name"],
        rest_api_id=PARAMETER["rest_api_id"],
        resource_id=PARAMETER["resource_id"],
    )
    assert ret["result"]
    assert ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.deployment",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )
    if not __test:
        PARAMETER.pop("resource_id")


@pytest.mark.asyncio
@pytest.mark.localstack(pro=False)
@pytest.mark.dependency(name="absent_with_none_resource_id", depends=["already_absent"])
async def test_deployment_absent_with_none_resource_id(hub, ctx):
    "idem-deployment-" + str(int(time.time()))
    # Delete Deployment with resource_id as None. Result in no-op.
    ret = await hub.states.aws.apigateway.deployment.absent(
        ctx,
        name=PARAMETER["name"],
        rest_api_id=PARAMETER["rest_api_id"],
        resource_id=None,
    )
    assert ret["result"], ret["comment"]
    assert (not ret["old_state"]) and (not ret["new_state"])
    assert (
        hub.tool.aws.comment_utils.already_absent_comment(
            resource_type="aws.apigateway.deployment",
            name=PARAMETER["name"],
        )[0]
        in ret["comment"]
    )


@pytest.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.apigateway.deployment.absent(
                ctx,
                name=PARAMETER["name"],
                rest_api_id=PARAMETER["rest_api_id"],
                resource_id=PARAMETER["resource_id"],
            )
            assert ret["result"], ret["comment"]
