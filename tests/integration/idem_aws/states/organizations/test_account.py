import uuid
from collections import ChainMap

import pytest
import pytest_asyncio

PARAMETRIZE = dict(argnames="__test", argvalues=[True, False], ids=["--test", "run"])
PARAMETER = {
    "name": "idem-test-acc" + str(uuid.uuid4()),
    "email": "idem-test-email-" + str(uuid.uuid4()) + "@example.com",
    "role_name": "idem-test-aws-role-" + str(uuid.uuid4()),
    "iam_user_access_to_billing": "ALLOW",
    "tags": {"org": "idem", "env": "test"},
}


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="present")
@pytest.mark.localstack(pro=True)
async def test_present(hub, ctx, __test, cleanup, aws_organization_unit):
    global PARAMETER
    ctx["test"] = __test
    parent_id = aws_organization_unit.get("resource_id", None)
    PARAMETER["parent_id"] = parent_id
    present_ret = await hub.states.aws.organizations.account.present(ctx, **PARAMETER)
    assert present_ret["result"], present_ret["comment"]
    resource = present_ret["new_state"]
    if __test:
        assert (
            hub.tool.aws.comment_utils.would_create_comment(
                resource_type="aws.organizations.account",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
    else:
        resource_id = resource["resource_id"]
        PARAMETER["resource_id"] = resource_id
        assert (
            hub.tool.aws.comment_utils.create_comment(
                resource_type="aws.organizations.account",
                name=PARAMETER["name"],
            )[0]
            in present_ret["comment"]
        )
        assert (
            f"Successfully moved aws.organizations.account {resource_id} under {parent_id}"
            in present_ret["comment"]
        )
        list_accounts = (
            await hub.exec.boto3.client.organizations.list_accounts_for_parent(
                ctx, ParentId=parent_id
            )
        )

        accounts = list_accounts["ret"].get("Accounts")
        filtered = list(
            filter(
                lambda acc: acc["Id"] == resource["resource_id"]
                and acc["Arn"] == resource["arn"],
                accounts,
            )
        )
        assert filtered
        assert filtered[0]["Id"] == resource["resource_id"]
        assert filtered[0]["Arn"] == resource["arn"]
        assert filtered[0]["Name"] == resource["name"]
        assert filtered[0]["Email"] == resource["email"]
        assert filtered[0]["Status"] == resource["status"]
    # assert statements
    assert PARAMETER["name"] == resource.get(
        "name"
    ), "Expected and Actual account_name should be equal"
    assert PARAMETER["email"] == resource.get(
        "email"
    ), "Expected and Actual email should be equal"
    assert PARAMETER["tags"] == resource.get("tags")
    assert PARAMETER["parent_id"] == resource.get("parent_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="update", depends=["present"])
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_update_parent_and_tags(hub, ctx, __test):
    # update account's parent id and tags
    ctx["test"] = __test
    list_roots_resp = await hub.exec.boto3.client.organizations.list_roots(ctx)
    destination_parent_id = list_roots_resp["ret"]["Roots"][0]["Id"]
    updated_tags = {"org": "idem-aws", "state": "temp"}
    PARAMETER["tags"] = updated_tags
    PARAMETER["parent_id"] = destination_parent_id
    update_ret = await hub.states.aws.organizations.account.present(ctx, **PARAMETER)

    await assert_account(
        hub,
        ctx,
        update_ret,
        PARAMETER["name"],
        PARAMETER["email"],
        updated_tags,
        destination_parent_id,
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="update-tags", depends=["present"])
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.localstack(pro=True)
async def test_update_tags(hub, ctx, __test):
    updated_tags_2 = {"org": "idem-aws-acc", "env": "sandbox"}
    PARAMETER["tags"] = updated_tags_2
    update_with_no_parent = await hub.states.aws.organizations.account.present(
        ctx, **PARAMETER
    )
    await assert_account(
        hub,
        ctx,
        update_with_no_parent,
        PARAMETER["name"],
        PARAMETER["email"],
        updated_tags_2,
        PARAMETER["parent_id"],
    )


@pytest.mark.asyncio
@pytest.mark.dependency(name="describe", depends=["present"])
@pytest.mark.localstack(pro=True)
async def test_describe(hub, ctx):
    describe_state = await hub.states.aws.organizations.account.describe(ctx)

    assert PARAMETER["resource_id"] in describe_state
    state = describe_state.get(PARAMETER["resource_id"]).get(
        "aws.organizations.account.present"
    )
    described_resource_map = dict(ChainMap(*state))
    assert PARAMETER["name"] == described_resource_map["name"]
    assert PARAMETER["email"] == described_resource_map["email"]
    assert PARAMETER["resource_id"] == described_resource_map["resource_id"]
    assert PARAMETER["tags"] == described_resource_map["tags"]
    assert PARAMETER["parent_id"] == described_resource_map["parent_id"]


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-get", depends=["present"])
@pytest.mark.localstack(pro=True)
async def test_exec_get(hub, ctx):
    ret = await hub.exec.aws.organizations.account.get(
        ctx, resource_id=PARAMETER["resource_id"]
    )
    assert ret["result"], ret["comment"]
    assert ret["ret"]
    resource = ret["ret"]
    assert PARAMETER["name"] == resource.get("name")
    assert PARAMETER["email"] == resource.get("email")
    assert PARAMETER["resource_id"] == resource.get("resource_id")


@pytest.mark.asyncio
@pytest.mark.dependency(name="exec-list", depends=["present"])
@pytest.mark.localstack(pro=True)
async def test_exec_list(hub, ctx):
    ret = await hub.exec.aws.organizations.account.list(ctx)
    assert ret["result"], ret["comment"]
    list_response = ret["ret"]
    assert list_response
    assert len(list_response) > 1


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="absent")
@pytest.mark.localstack(pro=True)
async def test_absent(hub, ctx, __test):
    ctx["test"] = __test
    resource_id = PARAMETER["resource_id"]
    absent_ret = await hub.states.aws.organizations.account.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert absent_ret["old_state"] and not absent_ret["new_state"]
    resource = absent_ret["old_state"]
    describe_after_delete = (
        await hub.states.aws.organizations.organization_unit.describe(ctx)
    )
    assert resource["resource_id"] not in describe_after_delete


@pytest.mark.asyncio
@pytest.mark.parametrize(**PARAMETRIZE)
@pytest.mark.dependency(name="already_absent", depends=["absent"])
@pytest.mark.localstack(pro=True)
async def test_already_absent(hub, ctx, __test):
    ctx["test"] = __test
    resource_id = PARAMETER["resource_id"]
    absent_ret = await hub.states.aws.organizations.account.absent(
        ctx,
        name=PARAMETER["name"],
        resource_id=resource_id,
    )
    assert (not absent_ret["old_state"]) and (not absent_ret["new_state"])
    if not __test:
        PARAMETER.pop("resource_id")


# This cleanup fixture cleans up the resource after all tests have run
@pytest_asyncio.fixture(scope="module")
async def cleanup(hub, ctx):
    global PARAMETER
    yield None
    if not hub.tool.utils.is_running_localstack(ctx):
        if "resource_id" in PARAMETER:
            ret = await hub.states.aws.organizations.account.absent(
                ctx,
                name=PARAMETER["name"],
                resource_id=PARAMETER["resource_id"],
            )
            assert ret["result"], ret["comment"]


async def assert_account(
    hub,
    ctx,
    update_ret,
    expected_account_name,
    expected_email,
    updated_tags,
    destination_parent_id,
):
    before_update_state = update_ret.get("old_state")
    after_update_state = update_ret.get("new_state")

    assert before_update_state and after_update_state

    assert (
        after_update_state["name"] == expected_account_name
    ), "Expected and Actual account_name should be equal "
    assert (
        after_update_state["email"] == expected_email
    ), "Expected and Actual email should be equal"

    assert after_update_state["tags"] == updated_tags
    assert after_update_state["parent_id"] == destination_parent_id

    if not ctx.get("test", False):
        list_accounts_for_new_parent = (
            await hub.exec.boto3.client.organizations.list_accounts_for_parent(
                ctx, ParentId=destination_parent_id
            )
        )

        accounts = list_accounts_for_new_parent["ret"].get("Accounts")

        filtered_after_update = list(
            filter(
                lambda acc: acc["Id"] == after_update_state["resource_id"]
                and acc["Arn"] == after_update_state["arn"],
                accounts,
            )
        )

        assert filtered_after_update
        assert filtered_after_update[0]["Id"] == after_update_state["resource_id"]
        assert filtered_after_update[0]["Arn"] == after_update_state["arn"]
