"""
Test creation and deletion of an instance with security groups
"""
import pytest


@pytest.mark.localstack(False)
@pytest.mark.dependency(name="present")
@pytest.mark.asyncio
async def test_present(
    hub,
    ctx,
    instance_name,
    aws_ec2_instance_type,
    aws_ec2_ami_pro,
    aws_ec2_security_group_in_default_vpc,
    aws_ec2_subnet_default,
    instance,
):
    """
    Create an instance with security group and subnet
    """
    ret = await hub.states.aws.ec2.instance.present(
        ctx,
        name=instance_name,
        # assign a newly created subnet to the instance
        subnet_id=aws_ec2_subnet_default.get("SubnetId"),
        # assign a newly created security group to the instance (using a security group in the same VPC as the subnet
        # because the instance's security group and subnet should belong to the same network)
        security_group_ids=[aws_ec2_security_group_in_default_vpc["resource_id"]],
        instance_type=aws_ec2_instance_type["resource_id"],
        image_id=aws_ec2_ami_pro["resource_id"],
        client_token=instance_name,
        tags={"Name": instance_name},
    )

    assert ret["result"], ret["comment"]
    assert ret["new_state"], ret["comment"]
    verify_instance(
        ret["new_state"], aws_ec2_security_group_in_default_vpc, aws_ec2_subnet_default
    )
    if ctx.test:
        return

    instance["resource_id"] = ret["new_state"]["resource_id"]

    # Wait for the resource to exist and be running
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Instance", ret["new_state"]["resource_id"]
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_exists")
    await hub.tool.boto3.resource.exec(resource, "wait_until_running")

    # Verify that we can perform a successful "get" and that the id matches the present new_state
    get = await hub.exec.aws.ec2.instance.get(
        ctx, resource_id=ret["new_state"]["resource_id"]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    assert get.ret["resource_id"] == ret["new_state"]["resource_id"]


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_get(
    hub,
    ctx,
    instance_name,
    aws_ec2_security_group_in_default_vpc,
    aws_ec2_subnet_default,
    instance,
):
    """
    Verify that "get" is successful after an instance has been created
    """
    get = await hub.exec.aws.ec2.instance.get(
        ctx, filters=[{"Name": "tag:Name", "Values": [instance_name]}]
    )
    assert get.result, get.comment
    assert get.ret, get.comment
    verify_instance(
        get.ret, aws_ec2_security_group_in_default_vpc, aws_ec2_subnet_default
    )

    # Verify that the instance id matches for both
    assert instance["resource_id"] == get.ret["resource_id"]


@pytest.mark.localstack(False)
@pytest.mark.dependency(depends=["present"])
@pytest.mark.asyncio
async def test_list(
    hub,
    ctx,
    instance_name,
    aws_ec2_security_group_in_default_vpc,
    aws_ec2_subnet_default,
    instance,
):
    """
    Verify that "list" is successful after an instances has been created
    """
    ret = await hub.exec.aws.ec2.instance.list(
        ctx, filters=[{"Name": "instance-id", "Values": [instance.resource_id]}]
    )
    assert ret.result, ret.comment
    assert ret.ret, ret.comment
    # Verify that the created instance is in the list
    assert ret.ret[0]["resource_id"] == instance.resource_id
    verify_instance(
        ret.ret[0], aws_ec2_security_group_in_default_vpc, aws_ec2_subnet_default
    )


@pytest.mark.localstack(
    False, "Localstack Bug: https://github.com/localstack/localstack/issues/6076"
)
@pytest.mark.asyncio
async def test_describe(
    hub,
    ctx,
    instance_name,
    aws_ec2_security_group_in_default_vpc,
    aws_ec2_subnet_default,
    instance,
):
    """
    Describe all instances and run the "present" state the described instance created for this module.
    No changes should be made and present/search/describe should have equivalent parameters.
    """
    # Describe all instances
    ret = await hub.states.aws.ec2.instance.describe(ctx)
    assert instance.resource_id in ret

    # Run the present state for our resource created by describe
    instance_kwargs = {}
    for pair in ret[instance.resource_id]["aws.ec2.instance.present"]:
        instance_kwargs.update(pair)

    # Run the present state on the result of "describe, no changes should be made
    instance_ret = await hub.states.aws.ec2.instance.present(ctx, **instance_kwargs)

    assert instance_ret["result"], instance_ret["comment"]

    # No changes should have been made!
    # We just created this state from describe
    assert instance_ret["old_state"] == instance_ret["new_state"]
    verify_instance(
        instance_ret["new_state"],
        aws_ec2_security_group_in_default_vpc,
        aws_ec2_subnet_default,
    )
    assert not instance_ret["changes"]


@pytest.mark.localstack(False)
@pytest.mark.asyncio
async def test_absent(hub, ctx, instance_name, instance):
    """
    Destroy the instance created by the present state
    """
    ret = await hub.states.aws.ec2.instance.absent(
        ctx, name=instance_name, resource_id=instance.resource_id
    )
    assert ret["result"], ret["comment"]
    assert ret["new_state"]

    # Wait until terminated so the subnet doesn't have any leftover dependencies
    resource = await hub.tool.boto3.resource.create(
        ctx, "ec2", "Instance", instance.resource_id
    )
    await hub.tool.boto3.resource.exec(resource, "wait_until_terminated")


def verify_instance(instance_state, expected_security_group, expected_subnet):
    assert (
        instance_state["subnet_id"] == expected_subnet["SubnetId"]
    ), "Instance is not created with the right subnet"
    assert instance_state[
        "security_group_ids"
    ], "Instance does not have security group IDs"
    assert (
        len(instance_state["security_group_ids"]) == 1
    ), "Instance should have only one security group"
    assert (
        instance_state["security_group_ids"][0]
        == expected_security_group["resource_id"]
    ), "Instance is not created with the right security group"
