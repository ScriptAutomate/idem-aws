def test_build(hub):
    arn = hub.tool.aws.arn_utils.build(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource" == arn

    arn = hub.tool.aws.arn_utils.build(
        service="fakeservice",
        region="fakeregion",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert "arn:aws:fakeservice:fakeregion:fakeaccountid:fakeresource" == arn

    arn = hub.tool.aws.arn_utils.build(
        partition="fakepartition",
        service="fakeservice",
        account_id="fakeaccountid",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice::fakeaccountid:fakeresource" == arn

    arn = hub.tool.aws.arn_utils.build(
        partition="fakepartition",
        service="fakeservice",
        region="fakeregion",
        resource="fakeresource",
    )
    assert "arn:fakepartition:fakeservice:fakeregion::fakeresource" == arn

    arn = hub.tool.aws.arn_utils.build(
        service="fakeservice",
        resource="fakeresource",
    )
    assert "arn:aws:fakeservice:::fakeresource" == arn


def test_parse(hub):
    parts = hub.tool.aws.arn_utils.parse(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource"
    )
    assert "fakepartition" == parts["partition"]
    assert "fakeservice" == parts["service"]
    assert "fakeregion" == parts["region"]
    assert "fakeaccountid" == parts["account_id"]
    assert "fakeresource" == parts["resource"]

    parts = hub.tool.aws.arn_utils.parse("arn:fakepartition:fakeservice:::fakeresource")
    assert "fakepartition" == parts["partition"]
    assert "fakeservice" == parts["service"]
    assert "" == parts["region"]
    assert "" == parts["account_id"]
    assert "fakeresource" == parts["resource"]

    parts = hub.tool.aws.arn_utils.parse(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:/fakeresourcetype/fakeresourcename"
    )
    assert "fakepartition" == parts["partition"]
    assert "fakeservice" == parts["service"]
    assert "fakeregion" == parts["region"]
    assert "fakeaccountid" == parts["account_id"]
    assert "/fakeresourcetype/fakeresourcename" == parts["resource"]

    parts = hub.tool.aws.arn_utils.parse(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresourcetype:fakeresourcename:fakeresoucequalifier"
    )
    assert "fakepartition" == parts["partition"]
    assert "fakeservice" == parts["service"]
    assert "fakeregion" == parts["region"]
    assert "fakeaccountid" == parts["account_id"]
    assert "fakeresourcetype:fakeresourcename:fakeresoucequalifier" == parts["resource"]

    try:
        hub.tool.aws.arn_utils.parse(
            "arn:fakepartition:fakeservice:fakeresion:fakeaccountid"
        )
    except TypeError as e:
        assert e
        assert "Invalid AWS ARN" in e.args[0]


def test_get_resource_name(hub):
    resource_name = hub.tool.aws.arn_utils.get_resource_name(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresourcetype:fakeresourcename:fakeresoucequalifier"
    )
    assert "fakeresourcename" == resource_name

    resource_name = hub.tool.aws.arn_utils.get_qualifier(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresourcetype"
    )
    assert "" == resource_name

    resource_name = hub.tool.aws.arn_utils.get_qualifier(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid"
    )
    assert "" == resource_name


def test_get_qualifier(hub):
    qualifier = hub.tool.aws.arn_utils.get_qualifier(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresourcetype:fakeresourcename:fakeresoucequalifier"
    )
    assert "fakeresoucequalifier" == qualifier

    qualifier = hub.tool.aws.arn_utils.get_qualifier(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresourcetype:fakeresourcename"
    )
    assert "" == qualifier

    qualifier = hub.tool.aws.arn_utils.get_qualifier(
        "arn:fakepartition:fakeservice:fakeregion:fakeaccountid:fakeresource"
    )
    assert "" == qualifier
